# docker-mini-project



## Description

This repository contains the files needed for the mini-project from INF's Docker Tutorial.

## Usage

Two containers sould be implemented:
- One using ctornau/latex image, which will convert the files from the latex folder into PDFs in the pdfs folder
- One using httpd image, which will host the index.html as a webservice, accessible through ´http://localhost:<SOME PORT (e.g. 8081)>´

## Tips
- The command to run the latexmk for converting the pdfs is ´latexmk -pdf /latex/*.tex -outdir=<OUTPUT DIRECTORY>´, the command to clean the auxiliary files is ´latexmk -c *.pdf"´, and all together, to generate the files and clean the auxiliaries, one could run ´latexmk -pdf /latex/*.tex -outdir=<OUTPUT DIR> && cd <OUTPUT DIR> && latexmk -c *.pdf"´
- It might be advisable to run the http webservice container in "daemon" (detached) mode, adding -d to the ´docker run´ command.